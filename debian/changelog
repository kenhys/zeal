zeal (1:0.6.1-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Backport upstream patch to fix build with Qt 5.15 (closes: #972177).

 -- Dmitry Shachnev <mitya57@debian.org>  Sun, 25 Oct 2020 13:29:32 +0300

zeal (1:0.6.1-1) unstable; urgency=medium

  * New upstream release.
  * Change Section to doc.

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Wed, 31 Oct 2018 14:44:09 +0800

zeal (1:0.6.0-2) unstable; urgency=medium

  * Update Build-Depends.
    * Add missing extra-cmake-modules (Closes: #890917).
    * Use asciidoc-base instead of asciidoc.
    * Remove qt5-qmake.

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Wed, 21 Feb 2018 15:57:55 +0800

zeal (1:0.6.0-1) unstable; urgency=medium

  * New upstream release (Closes: #801125).
  * Bump Standards-Version to 4.1.3.
  * Bump compat to 11.
  * Update Vcs-* fields to salsa.debian.org.
  * Disable ad by default (Closes: #878874).

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Tue, 20 Feb 2018 22:29:08 +0800

zeal (1:0.4.0-2) unstable; urgency=medium

  * Add missing Build-Depends libsqlite3-dev (Closes: #876606).

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Sat, 07 Oct 2017 15:07:40 +0800

zeal (1:0.4.0-1) unstable; urgency=medium

  * New upstream release (Closes: 875614).
  * Bump Standards-Version to 4.1.0.
    * Change priority to optional.
    * Use https in Format field in d/copyright.
  * Bump compat to 10.
  * Don't set ZEAL_VERSION in d/rule since it is set by qmake/common.pri.

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Sat, 16 Sep 2017 17:23:13 +0800

zeal (1:0.3.1-1) unstable; urgency=medium

  * New upstream release.
  * Update watch file.

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Fri, 14 Oct 2016 22:35:40 +0800

zeal (1:0.2.1-4) unstable; urgency=medium

  * Use asciidoc instead of ronn to generate manpage.

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Sun, 07 Aug 2016 01:03:02 +0800

zeal (1:0.2.1-3) unstable; urgency=medium

  * Use UTC time to make package reproducible.
    Thanks to Chris Lamb (Closes: 828164)

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Sun, 26 Jun 2016 00:18:55 +0800

zeal (1:0.2.1-2) unstable; urgency=medium

  * Bump Standards-Version to 3.9.8.
  * Remove zeal-dbg in favor of zeal-dbgsym.
  * Update Vcs-* fields.

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Wed, 15 Jun 2016 09:31:02 +0800

zeal (1:0.2.1-1) unstable; urgency=medium

  * New upstream release.

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Thu, 26 Nov 2015 20:06:48 +0800

zeal (1:0.2.0-1) unstable; urgency=medium

  * New upstream release.

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Tue, 17 Nov 2015 12:15:18 +0800

zeal (1:0.1.1+20151029-2) unstable; urgency=medium

  * Adjust priority to extra for libxcb-keysyms1.
  * Update Vcs-* fields.

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Tue, 03 Nov 2015 13:05:42 +0800

zeal (1:0.1.1+20151029-1) unstable; urgency=medium

  [ Yao-Po Wang ]
  * Add -D_FORTIFY_SOURCE=2 into CXXFLAGS to avoid the dpkg-buildflags-missing
    warning in blhc. (Closes: #795843)

  [ ChangZhuo Chen (陳昌倬) ]
  * Use upstream git snapshot. (Closes: #802871)
  * Update copyright.

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Sun, 01 Nov 2015 20:01:40 +0800

zeal (1:0.1.1-2) unstable; urgency=medium

  * Add missing dependency libqt5sql5-sqlite.
    Thanks to Sammy Tahtah (Closes: #795561)
  * Set ZEAL_VERSION.
    Thanks to Sammy Tahtah (Closes: #795564)

 -- ChangZhuo Chen (陳昌倬) <czchen@gmail.com>  Mon, 17 Aug 2015 00:14:11 +0800

zeal (1:0.1.1-1) unstable; urgency=medium

  * New upstream release
  * Remove unused libquazip-qt5 patch
  * Update copyright

 -- ChangZhuo Chen (陳昌倬) <czchen@gmail.com>  Thu, 23 Jul 2015 21:23:15 +0800

zeal (20141123-1) unstable; urgency=low

  * Initial release (Closes: #747219)

 -- ChangZhuo Chen (陳昌倬) <czchen@gmail.com>  Thu, 19 Feb 2015 22:18:16 +0800
